TEMPLATE = app
TARGET = YandereRenamer
DEPENDPATH += . src
INCLUDEPATH += .
QT += widgets
QT += network

# Input
HEADERS += src/mainwindow.h src/pugixml/pugixml.hpp  src/pugixml/pugiconfig.hpp src/file.h src/file_model.h
FORMS += src/mainwindow.ui
SOURCES += src/main.cpp src/mainwindow.cpp src/pugixml/pugixml.cpp src/file.cpp src/file_model.cpp
