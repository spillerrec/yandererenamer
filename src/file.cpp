/*
	This file is part of YandereRenamer.

	YandereRenamer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	YandereRenamer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with YandereRenamer.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "file.h"

#include <QFile>
#include <QFileInfo>
#include <QRegExp>

#include "pugixml/pugixml.hpp"
using namespace pugi;

file::file( QString path ){
	QFileInfo f( path );
	dir = f.absolutePath() + "/";
	name = f.completeBaseName(); //TODO: properly keep .preview.ext, but avoid Konachan.com issue
	ext = f.suffix();
	if( !ext.isEmpty() )
		ext = "." + ext;
	
	//Try to find some way of intentifying the string
	QString base = f.completeBaseName();
	id = -1;
	
	//Check for a md5 hash
	QRegExp md5_check( "^[0-9a-zA-Z]{32}$" );
	if( md5_check.exactMatch( base ) )
		hash = base;
	else{	//Try to find a site and ID
		//site is from the beginning to the first space
		int pos = base.indexOf( " " );
		if( pos != -1 ){
			site = base.left( pos );
			base = base.remove( 0, pos + 1 );
		}
		
		//Konachan has a different formating, so handle this differently
		if( site == "Konachan.com" ){
			site = "kona"; //change to known shorthand
			base = base.remove( 0, 2 ); //Remove "- " from string
		}
		//Change shorthand for Yande.re files
		else if( site == "yande.re" || site == "moe" )
			site = "yandere";
		
		//Now try to find the ID
		pos = base.indexOf( " " );
		if( pos != -1 )
			id = base.left( pos ).toInt();
		
		//Reset if id invalid
		if( id <= 0 ){
			id = -1;
			site = "";
		}
	}
}


//Rename the file on the filesystem
bool file::rename(){
	bool succeeded = false;
	if( can_rename() ){
		QString new_name = dir + full_name + ext;
		QString old_name = dir + name + ext;
		succeeded = QFile::rename( old_name, new_name );
		
		//Change the name if the renamed succeeded
		if( succeeded )
			name = full_name;
		else{
			QFileInfo old( new_name );
			if( old.exists() )
				if( old.size() == QFileInfo( old_name ).size() )
					QFile::remove( old_name );
		}
	}
	return succeeded;
}


//Return an QUrl which will respond with data 'parse_contents()' can use
QUrl file::contents_url() const{
	if( !is_valid() )
		return QUrl();
	
	if( id > 0 )
		return QUrl( "http://booru.localhost/tool/filename/" + site + "/" + QString::number( id ) + "/" );
	else
		return QUrl( "http://booru.localhost/tool/md5/" + hash + "/" );
}


//Parse contents returned by the service at 'contents_url()'
void file::parse_contents( QByteArray data ){
	xml_document doc;
	if( doc.load_buffer( data.data(), data.size() ) ){
		//Check if request was succesful
		xml_node error_node = doc.child( "error" );
		if( error_node ){
			error = QString::fromUtf8( error_node.child_value() );
			return;
		}
		
		//Get data from response
		QString hash_check = QString::fromUtf8( doc.child( "post" ).child( "hash" ).child_value() );
		if( hash.isEmpty() || hash_check == hash )
			full_name = QString::fromUtf8( doc.child( "post" ).child( "filename" ).child_value() );
		else
			error = QObject::tr( "Hash check failed!" );
	}
	else
		error = QObject::tr( "Could not parse XML response" );
}

