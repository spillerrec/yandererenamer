/*
	This file is part of YandereRenamer.

	YandereRenamer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	YandereRenamer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with YandereRenamer.  If not, see <http://www.gnu.org/licenses/>.
*/

/*	Controls how a 'std::vector<file>' is displayed in a
 *	QModelView or similar
 */

#ifndef FILE_MODEL_H
#define FILE_MODEL_H

#include "file.h"
#include <vector>
#include <QAbstractListModel>


class file_model: public QAbstractListModel{
	private:
		std::vector<file> *list;
	
	public:
		explicit file_model( std::vector<file> *list_to_model ){
			list = list_to_model;
		}
	
	//Implementation of QAbstractListModel
	public:
		int rowCount( const QModelIndex &parent = QModelIndex() ) const{
			return ( list && !parent.isValid() ) ? list->size() : 0;
		}
		int columnCount( const QModelIndex &parent = QModelIndex() ) const{ return 3; }
		
		QVariant headerData( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
		QVariant	data( const QModelIndex &index, int role = Qt::DisplayRole ) const;
		
	public slots:
		void update_row( unsigned row );
};

#endif

