/*
	This file is part of YandereRenamer.

	YandereRenamer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	YandereRenamer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with YandereRenamer.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "ui_mainwindow.h"
#include "mainwindow.h"
#include "file.h"

#include <QUrl>
#include <QDragEnterEvent>
#include <QDropEvent>

main_widget::main_widget(): QMainWindow(), ui(new Ui_main_widget), files_model( &files ), manager( this ){
	ui->setupUi(this);
	connect( ui->btn_rename, SIGNAL( clicked() ), this, SLOT( rename_files() ) );
	connect( ui->btn_clear, SIGNAL( clicked() ), this, SLOT( clear() ) );
	
	connect( &manager, SIGNAL( finished(QNetworkReply*) ), this, SLOT( handle_response(QNetworkReply*) ) );
	setAcceptDrops( true );
	
	ui->list_files->setModel( &files_model );
}


//Handle response from Network request and update model
void main_widget::handle_response( QNetworkReply* reply ){
	unsigned pos = reply->request().attribute( QNetworkRequest::User ).toInt();
	
	//Don't access out-of-bounds
	//Still wouldn't work as expected if list was changed,
	//however it prevents crashing
	if( pos < files.size() ){
		files.at( pos ).parse_contents( reply->readAll() );
		files_model.update_row( pos );
	}
	
	reply->deleteLater();
}


void main_widget::clear(){
	files.clear();
	ui->list_files->reset();
}


//Rename all files in list
void main_widget::rename_files(){
	for( unsigned i=0; i<files.size(); i++ )
		if( files[i].can_rename() ){
			files[i].rename();
			files_model.update_row( i );
		}
}


//Accept drop events
void main_widget::dragEnterEvent( QDragEnterEvent *event ){
	if( event->mimeData()->hasUrls() )
		event->acceptProposedAction();
}


//Handle drop events
void main_widget::dropEvent( QDropEvent *event ){
	if( event->mimeData()->hasUrls() ){
		event->setDropAction( Qt::CopyAction );
		event->accept();
		
		//Add files
		foreach( QUrl url, event->mimeData()->urls() ){
			files.push_back( file( url.toLocalFile() ) );
			
			//Get required content
			int i = files.size() -1;
			QNetworkRequest request( files[i].contents_url() );
			request.setAttribute( QNetworkRequest::User, i );
			manager.get( (const QNetworkRequest)request );
		}
		
		ui->list_files->reset();
	}
}

