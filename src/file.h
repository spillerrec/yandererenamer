/*
	This file is part of YandereRenamer.

	YandereRenamer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	YandereRenamer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with YandereRenamer.  If not, see <http://www.gnu.org/licenses/>.
*/

/*	Defines a file and how it can be renamed
 */

#ifndef FILE_H
#define FILE_H

#include <QString>
#include <QUrl>


class file{
	private:
		QString dir;	//Directory the file resides in, with ending '/'
		QString name;	//file name, without extension
		QString ext;	//extension of the file
		
		int id;
		QString site;
		QString hash;
		QString full_name;
		QString error;
	
	public:
		QString get_name() const{ return name; }
		QString get_full_name() const{ return full_name; }
		QString get_id() const{ return id > 0 ? site + " " + QString::number(id) : hash; }
		QString get_last_error() const{ return error; }
	
	public:
		explicit file( QString path );
		
		bool is_valid() const{
			return id > 0 || !hash.isEmpty();
		}
		bool can_rename() const{
			return !full_name.isEmpty() && name != full_name;
		}
		bool rename();
		
		QUrl contents_url() const;
		
		void parse_contents( QByteArray data );
};

#endif

