/*
	This file is part of YandereRenamer.

	YandereRenamer is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	YandereRenamer is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with YandereRenamer.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "file_model.h"

#include <QColor>
#include <QBrush>

QVariant	file_model::data( const QModelIndex &index, int role ) const{
	if( index.row() < rowCount() && index.row() >= 0 ){
		if( role == Qt::DisplayRole ){
			//Return the data in that field
			file& row = list->at( index.row() );
			switch( index.column() ){
				case 0: return row.get_id();
				case 1: return row.get_name();
				case 2: return ( row.get_last_error().isEmpty() ) ? row.get_full_name() : row.get_last_error();
			}
		}
		else if( role == Qt::BackgroundRole ){
			//Return the background color, for a whole row
			file& row = list->at( index.row() );
			
			//Color depending on 'row' state
			if( !row.is_valid() )
				return QBrush( QColor( 255, 0, 0 ) );
			
			if( !row.get_last_error().isEmpty() )
				return QBrush( QColor( 255, 255, 0 ) );
			
			if( row.can_rename() )
				return QBrush( QColor( 0, 255, 0 ) );
		}
	}
	
	return QVariant();
}


QVariant file_model::headerData( int section, Qt::Orientation orientation, int role ) const{
	if( role == Qt::DisplayRole ){
		if( orientation != Qt::Horizontal )
			return QVariant();
		
		//Return the header name for each column
		switch( section ){
			case 0: return tr( "Id" );
			case 1: return tr( "Filename" );
			case 2: return tr( "New filename" );
			
			default: return QVariant();
		}
	}
	
	return QVariant();
}


//Update a single row in the model
void file_model::update_row( unsigned row ){
	int coloums = rowCount( index( row, 0 ) );
	emit dataChanged( index( row, 0 ), index( row+1, coloums ) );
}

